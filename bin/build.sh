#!/usr/bin/env bash

set -e
set -x

if ! type -p xelatex >/dev/null ; then
  echo "Please install xelatex" >&2
  exit 127
fi

if ! type -p pandoc >/dev/null ; then
  echo "Please install pandoc" >&2
  exit 127
fi

if ! type -p gs >/dev/null ; then
  echo "Please install ghostscript" >&2
  exit 127
fi

if ! type -p convert >/dev/null ; then
  echo "Please install imagemagick" >&2
  exit 127
fi

if [ $# -lt 9 ]
  then
    echo "Supply arguments: CI_PAGES_DOMAIN CI_PAGES_URL CI_PROJECT_TITLE CI_PROJECT_URL COMMIT_TIME GITLAB_USER_NAME GITLAB_USER_EMAIL CI_COMMIT_SHA CI_PROJECT_VISIBILITY"
    exit 1
fi

script_dir=$(dirname "$0")
dist_dir=${script_dir}/../public
src_dir=${script_dir}/../src
etc_dir=etc

mkdir -p ${dist_dir}

rsync -aH ${etc_dir}/index.html ${dist_dir}

md_src_files=$(find -L ${src_dir} -maxdepth 1 -name '*.md' -type f | sort)

for md_src_file in ${md_src_files}; do
  md_src_file_basename=$(basename -- "${md_src_file}")
  (cat ${etc_dir}/logo.md && \
   echo -e "" && \
   echo -e "# Student take-home work" && \
   echo -e "\n----\n" && \
   cat ${md_src_file} && \
   echo -e "\n----\n" && \
   cat ${etc_dir}/student-resources.md && \
   echo -e "\n----\n" && \
   echo -e "### This document\n\n" && \
   echo -e "* hosted at **[${1}](${2})**\n" && \
   echo -e "* source hosted at **[${3}](${4})**\n" && \
   echo -e "* last updated at time **${5}**\n" && \
   echo -e "* last updated by user **[${6}](mailto:${7})**\n" && \
   echo -e "* revision **[${8}](${4}/-/commit/${8})**\n" && \
   echo -e "* access control **${9}**\n" && \
   echo -e "\n----\n" && \
   cat ${etc_dir}/licence.md &&\
   echo -e "\n----\n"
  ) > ${dist_dir}/${md_src_file_basename}
done

md_files=$(find -L ${dist_dir} -maxdepth 1 -name '*.md' -type f | sort)

for md_file in ${md_files}; do
  md_file_basename=$(basename -- "${md_file}")
  md_file_filename="${md_file_basename%.*}"
  echo ${md_file_filename}

  for format in pdf html docx odt; do
    echo "  markdown->${format}"
    pandoc --highlight-style=espresso -fmarkdown-implicit_figures -M mainfont="DejaVu Sans Mono" --pdf-engine=xelatex ${md_file} -o ${dist_dir}/${md_file_filename}.${format}
  done

  if [ -f ${dist_dir}/${md_file_filename}.pdf ]; then
    pdf_file=${md_file_filename}.pdf
    pdf_basename=$(basename -- "${pdf_file}")
    pdf_dirname=$(dirname -- "${pdf_file}")
    pdf_filename="${pdf_file%.*}"

    page1_pdf_dir=${dist_dir}/${pdf_dirname}
    page1_pdf="${page1_pdf_dir}/${pdf_filename}_page1.pdf"

    mkdir -p "${page1_pdf_dir}"

    gs -q -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dSAFER -dFirstPage=1 -dLastPage=1 -sOutputFile=${page1_pdf} ${dist_dir}/${pdf_file}

    page1_pdf_png="${page1_pdf}.png"
    convert ${page1_pdf} ${page1_pdf_png}

    for size in 600 450 350 250 150 100 65 45
    do
      page1_pdf_png_size="${page1_pdf}-${size}.png"
      convert ${page1_pdf_png} -resize ${size}x${size} ${page1_pdf_png_size}
    done
  fi
done
