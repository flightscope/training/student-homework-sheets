### Student Resources

Student resources can be found on the Flightscope Aviation website.

[http://flightscopeaviation.com.au/student-resources/](http://flightscopeaviation.com.au/student-resources/)

* Flightscope Aviation Aircraft flight manuals (PoH)
* Archerfield ERSA and aeronautical charts
* RA-Aus Operations Manual, Technical Manual and Syllabus of Flight Training
* Radio Practice cards and material
* CASA Visual Flight Rules Guide (VFRG)
* Airservices NAIPS
* Airservices Aeronautical Information Package (AIP) *(ERSA is a sub-document of the AIP)*

----

### Questions

If you have any questions or need any help with this work, please contact your flight instructor.
