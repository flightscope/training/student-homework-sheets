## Archerfield Operations

1. Using the ERSA for Brisbane/Archerfield:
  * What time does the control tower open and close?
  * What is the elevation of the airport?
  * What is the class of airspace during tower hours?
  * What is the class of airspace outside tower hours?
  * What runways are operational outside tower hours?
  * Where are helicopter areas A, B and C?
  * What is the circuit direction on runway 28R during tower hours?
  * What is the circuit direction on runway 28R outside tower hours?
  * What is the CTAF frequency?
  * At what altitude are all departures?
  * At what altitude are all arrivals?
  * What is the AWIS frequency?
  * What is the circuit altitude?
  * What is the magnetic track for an eastern departure?
  * What is the magnetic track for a southern departure?
  * What are the four arrival points for VFR aircraft?
  * Which Wind Direction Indicators (WDI) and Illuminated Wind Indicators (IWI) are illuminated at night?
  * What is the procedure for a communication failure, such as a radio failure?
  * After vacating runway 10L/28R at B3, what are the taxi and clearance requirements?
2. Using the Brisbane/Sunshine Coast Visual Terminal Chart (VTC):
  * What is the altitude at which Class C airspace starts at Target?
  * What are the markers with a small, black ellipse that have a flag protruding from the centre, and why might they be important?
  * What is the CTAF frequency at Heck Field?
  * What is the name and altitude of the restricted area, approximately 6NM south of Archerfield?
  * At what altitude does the Archerfield CTR stop?
  * What is the airspace class above the Archerfield CTR?
  * What is the maximum altitude, outside controlled airspace, at the Tingalpa Reservoir?
  * What is the name of the airfield near the north-western tip of North Stradbroke Island?
  * Locate 3 runs of powerlines.
  * What do the yellow sections represent?
  * What is the Brisbane Centre (BN CEN) frequency outside the immediate vicinity of the Archerfield CTR?
3. Using both the ERSA for Brisbane/Archerfield and the Brisbane/Sunshine Coast Visual Terminal Chart (VTC):
  * Locate danger area `D675` on the chart, and find the details for `D675` in the ERSA under PRD AREAS.
  * Locate VFR waypoint `ABM TV TOWERS` on the chart, and find the details, including its code, for `ABM TV TOWERS` in the ERSA under VFR WAYPOINTS.
