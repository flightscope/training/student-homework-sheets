## Area Solo

1. What documents are required to be carried onboard your aircraft?

2. What are the current and forecast meteorological conditions for your area solo?

3. What is the maximum altitude at Target without entering Class C airspace?

4. What is the altitude that we depart the Archerfield Control Zone?

5. What is the altitude that we arrive to the Archerfield Control Zone?

6. You call Archerfield Tower at Target inbound and you are instructed to "maintain 1500, join downwind RWY 10L"
  * what visual reference point(s) will you track for the Archerfield Control Zone?
  * when can you descend from 1500ft?

7. You call Archerfield Tower at Park Ridge Water Tower inbound and tower responds but does not provide join instructions. What do you do?

8. You choose to practice forced landings on your area solo.
  * What is the legal minimum altitude that you can go?
  * What is your personal minimum altitude that you will go?

9. You are south of Target at 2000ft AMSL and you sight an aircraft same-level head-on. What immediate actions do you take?

10. You are practicing near Heckfield airfield. How might you configure the radio to increase situational awareness?

11. What radio frequency do you change to after departing east from the Archerfield Control Zone?

12. What does squawk code 1200 mean?

13. You are inbound to Archerfield and have contacted the tower, however you see thick cloud in front of you. What is your plan?

14. Archerfield Tower has given you an instruction. It sounded important, but you didn't comprehend it all. How will you respond?

15. You have come in to Archerfield from Target, with instructions to join final RWY 28R, but have started your descent too late. You are high on final at 75KIAS. What is your plan?

16. You are 300ft on final but you just realised that you have forgotten to extend flaps. Describe your plan.

17. The flight controls do not feel correct and the aircraft is not responding normally to your inputs.
  * Is this an emergency?
  * What type of emergency?
  * What is your plan?

18. You have just departed Archerfield and your instructor has asked you to spend an hour or more practicing solo. However, you see that the cloud base is quickly getting lower and you do not feel comfortable. When will you return to Archerfield?
