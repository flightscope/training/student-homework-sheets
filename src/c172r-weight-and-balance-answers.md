## Cessna 172R Weight and Balance (incl answers)

Following are weight and balance exercises for a Cessna 172R. The Cessna 172R factory flight manual can be found on [the flightscopeaviation.com.au website under Student Resources → Aircraft Documentation](https://www.flightscopeaviation.com.au/aircraft-documentation). The weight and balance section of the flight manual is contained within Section 6.

1. Determine weight and balance limits for the following scenario:
  * **Aircraft Basic Empty Weight:** `761.8 kg`
  * **Aircraft Empty Moment:** `766.3 IU`
  * **Fuel onboard:** `56 USG` *(full)*
  * **Cabin Row 1:** `130 kg`
  * **Cabin Row 2:** `90 kg`
  * **Baggage Area A:** `0 kg`
  * **Baggage Area B:** `0 kg`

```haskell
{-
Ramp
  Weight: 1134.1 kg
  Moment: 1242.4 IU
  Fuel total: 211.6L
  Fuel usable: 200.2L
  Fuel endurance @ 40L/hr
    Total usable: 300min
    Incl reserve: 255min
ZFW
  Weight: 981.8 kg
  Moment: 1056.7 IU

ABOVE Maximum Ramp Weight
-}
```

2. Determine weight and balance limits for the following scenario:
  * **Aircraft Basic Empty Weight:** `761.8 kg`
  * **Aircraft Empty Moment:** `766.3 IU`
  * **Fuel onboard:** `150L`
  * **Cabin Row 1:** `100 kg`
  * **Cabin Row 2:** `120 kg`
  * **Baggage Area A:** `0 kg`
  * **Baggage Area B:** `0 kg`

```haskell
{-
Ramp
  Weight: 1089.8 kg
  Moment: 1215.8 IU
  Fuel total: 150L
  Fuel usable: 138.7L
  Fuel endurance @ 40L/hr:
    Total usable: 208min
    Incl reserve: 163min
ZFW
  Weight: 981.8 kg
  Moment: 1084.1 IU

WITHIN ALL LIMITS
-}
```

3. Determine weight and balance limits for the following scenario:
  * **Aircraft Basic Empty Weight:** `761.8 kg`
  * **Aircraft Empty Moment:** `766.3 IU`
  * **Fuel onboard:** `100 L`
  * **Cabin Row 1:** `60 kg`
  * **Cabin Row 2:** `150 kg`
  * **Baggage Area A:** `50 kg`
  * **Baggage Area B:** `10 kg`

```haskell
{-
Ramp
  Weight: 1103.8 kg
  Moment: 1341.8 IU
  Fuel total: 100L
  Fuel usable: 88.7L
  Fuel endurance @ 40L/hr:
    Total usable: 133min
    Incl reserve: 88min
ZFW
  Weight: 1031.8 kg
  Moment: 1254.0 IU

Within weight
Within baggage loading limits
OUTSIDE CG on both Ramp and ZFW
-}
```

4. You have a box for the baggage area, measuring `50cm x 20cm x 10cm`, weighing `12 kg`. Determine weight and balance limits for the following scenario where the box is placed into baggage area A:
  * **Aircraft Basic Empty Weight:** `761.8 kg`
  * **Aircraft Empty Moment:** `766.3 IU`
  * **Fuel onboard:** `160 L`
  * **Cabin Row 1:** `120 kg`
  * **Cabin Row 2:** `0 kg`
  * **Baggage Area A:** `12 kg`
  * **Baggage Area B:** `0 kg`

```haskell
{-
Ramp
  Weight: 1009.0 kg
  Moment: 1049.8 IU
  Fuel total: 160L
  Fuel usable: 148.7L
  Fuel endurance @ 40L/hr:
    Total usable: 227min
    Incl reserve: 182min
ZFW
  Weight: 893.8 kg
  Moment: 909.4 IU

Within weight
Within CG
Within baggage loading limits
OUTSIDE baggage loading intensity limits
-}
```

5. Determine weight and balance limits for the following scenario:
  * **Aircraft Basic Empty Weight:** `761.8 kg`
  * **Aircraft Empty Moment:** `766.3 IU`
  * **Fuel onboard:** `140 L`
  * **Cabin Row 1:** `100 kg`
  * **Cabin Row 2:** `0 kg`
  * **Baggage Area A:** `60 kg`
  * **Baggage Area B:** `25 kg`

```haskell
{-
Ramp
  Weight: 1047.6 kg
  Moment: 1207.4 IU
  Fuel total: 140L
  Fuel usable: 128.7L
  Fuel endurance @ 40L/hr:
    Total usable: 193min
    Incl reserve: 148min
ZFW
  Weight: 946.8 kg
  Moment: 1084.5 IU

Within weight
Within CG
OUTSIDE baggage loading limits
-}
```
