## Cessna 172R Weight and Balance

Following are weight and balance exercises for a Cessna 172R. The Cessna 172R factory flight manual can be found on [the flightscopeaviation.com.au website under Student Resources → Aircraft Documentation](https://www.flightscopeaviation.com.au/aircraft-documentation). The weight and balance section of the flight manual is contained within Section 6.

1. Determine weight and balance limits for the following scenario:
  * **Aircraft Basic Empty Weight:** `761.8 kg`
  * **Aircraft Empty Moment:** `766.3 IU`
  * **Fuel onboard:** `56 USG` *(full)*
  * **Cabin Row 1:** `130 kg`
  * **Cabin Row 2:** `90 kg`
  * **Baggage Area A:** `0 kg`
  * **Baggage Area B:** `0 kg`

2. Determine weight and balance limits for the following scenario:
  * **Aircraft Basic Empty Weight:** `761.8 kg`
  * **Aircraft Empty Moment:** `766.3 IU`
  * **Fuel onboard:** `150L`
  * **Cabin Row 1:** `100 kg`
  * **Cabin Row 2:** `120 kg`
  * **Baggage Area A:** `0 kg`
  * **Baggage Area B:** `0 kg`

3. Determine weight and balance limits for the following scenario:
  * **Aircraft Basic Empty Weight:** `761.8 kg`
  * **Aircraft Empty Moment:** `766.3 IU`
  * **Fuel onboard:** `100 L`
  * **Cabin Row 1:** `60 kg`
  * **Cabin Row 2:** `150 kg`
  * **Baggage Area A:** `50 kg`
  * **Baggage Area B:** `10 kg`

4. You have a box for the baggage area, measuring `50cm x 20cm x 10cm`, weighing `12 kg`. Determine weight and balance limits for the following scenario where the box is placed into baggage area A:
  * **Aircraft Basic Empty Weight:** `761.8 kg`
  * **Aircraft Empty Moment:** `766.3 IU`
  * **Fuel onboard:** `160 L`
  * **Cabin Row 1:** `120 kg`
  * **Cabin Row 2:** `0 kg`
  * **Baggage Area A:** `12 kg`
  * **Baggage Area B:** `0 kg`

5. Determine weight and balance limits for the following scenario:
  * **Aircraft Basic Empty Weight:** `761.8 kg`
  * **Aircraft Empty Moment:** `766.3 IU`
  * **Fuel onboard:** `140 L`
  * **Cabin Row 1:** `100 kg`
  * **Cabin Row 2:** `0 kg`
  * **Baggage Area A:** `60 kg`
  * **Baggage Area B:** `25 kg`
