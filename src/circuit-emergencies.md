## Circuit Emergencies

1. What are some causes of engine failure?

2. What are some reasons we might reject a take-off during the take-off roll?

3. What immediate action do we perform upon an engine failure?

4. During a flapless landing, is the nose attitude higher or lower?

5. What is the best glide speed of the Eurofox 3K?

6. What is a F.A.S.T. check?

7. The engine fails while turning crosswind. Describe your plan.

8. What does flap extension do to the angle of descent?

9. What is the greatest risk in attempting to turn back to the runway without power at low attitude?

10. The radio has failed on downwind. Describe your plan.
