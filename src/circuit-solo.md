## Circuit Solo

1. You are in the run-up bay performing an ignition check. However, after repeated attempts, you are unable to see a drop in RPM on the left ignition side. What are your next actions?

2. You are on circuit solo and you are close behind another landing aircraft. Tower has not cleared you to land. What are your next actions?

3. You are 100ft above the runway during take-off with flaps in take-off configuration. The engine fails. What are your next immediate actions?

4. On a Visual Terminal Chart, in some areas, there are small black ellipses with a small flag in the middle. What are these and why might they be useful? Are there any in or near the Archerfield circuit area?

5. On a circuit solo, the radio stops working. All other electrics are working. Describe your next actions.

6. What is the minimum altitude that an aircraft may begin a turn onto crosswind?

7. You are 100ft above the runway on take-off and tower requests of you, "request early left turn." However, you are not comfortable with this request. What are your next actions?

8. You are 300ft above the runway on take-off and tower provides an instruction, "traffic alert turn left immediately." What are your next actions?

8. At what level does the class D airspace extend to at Archerfield?

9. What does CTAF mean and does Archerfield ever operate under the CTAF procedures?
  * *type this into a search engine "AC 91-10"*
  * *see ERSA*

10. What is the elevation at Archerfield?
  * *see ERSA*

11. What does "wilco" mean in radio phraseology?
  * *see AIP GEN 2.2 GENERAL AND METEOROLOGICAL ABBREVIATIONS*

12. Which of these are unauthorised radio phraseology?
  * [ ] MAYDAY MAYDAY MAYDAY
  * [ ] Using plain English to communicate a situation
  * [ ] "You bloody idiot you nearly hit me!"
  * [ ] Using a language that is not English
  * [ ] PAN PAN

13. Why do we clean the windscreen of the aircraft before our flight?
  * [ ] The aircraft owner enjoys making students do extra physical work
  * [ ] It makes the view outside more enjoyable
  * [ ] It is a legal requirement per the aircraft flight manual to meet aircraft serviceability

14. During circuit solo at Archerfield, the responsibility of aircraft separation falls ultimately to:
  * [ ] The air traffic control tower
  * [ ] The Pilot in Command of the aircraft
  * [ ] The instructor waiting on the ground

15. What is the practical purpose of performing a **readback** on the radio?

16. The control tower has given you an instruction that you don't understand. What are your next actions?
  * [ ] Respond "Copy"
  * [ ] Attempt a readback of the instruction, despite not understanding it
  * [ ] Respond "Say again" and request clarification of the instruction
  * [ ] Respond "Yeah mate no worries" and try to meet the instruction as best as you possibly can

17. You are 300ft AGL on final with full flaps extended, power at idle and your airspeed is just below Vfe. You are also quite high on approach. Describe your next actions in detail.

18. You are turning base to final but you've turned late, crossing passed the runway extended centreline. What are your next actions?
  * [ ] Push further on the rudder into the turn to help with intercepting the runway extended centreline
  * [ ] Perform a lookout for any traffic conflicts, bank the aircraft using aileron to a comfortable limit, with appropriate rudder input to remain coordinated and re-intercept the centreline, then perform a Go-Around.
  * [ ] Apply power, climb and turn away from the runway, then inform the control tower of your intentions.

19. Your engine has failed on downwind and your F.A.S.T. checks have failed. You've maintained correct glide attitude on base, however, you now feel you are falling short of the runway. What are your next actions?
  * [ ] Raise the nose of the aircraft to extend your glide to the runway
  * [ ] Close the cabin air vents to improve the aerodynamic characteristics of the aircraft
  * [ ] Make a gentle coordinated turn at 5-10 degrees AoB toward the runway touchdown point
  * [ ] Make a safe, positive, coordinated turn at approximately 30 degrees AoB toward the runway extended centreline, re-intercepting a glide profile to the runway

20. Your instructor has asked you to do three solo circuits, however, on the first circuit you notice low oil pressure during your pre-landing checks. Describe your next actions.

21. On downwind, you see you have an engine **fire**. What are your next actions?
  * *see the Aircraft Flight Manual EMERGENCY PROCEDURES*

22. After startup, you receive the ATIS and set the QNH, but notice that it overreads by 180ft. Is it safe/legal to continue the flight? What if you receive the QNH from the METAR/SPECI?
  * *see AIP ENR 1.7 ALTIMETER SETTING PROCEDURES*

23. On downwind, you notice a strobe light circuit breaker has popped out, so you push it back in. However, it immediately pops out again. What type of fault might you have? What are your next actions?

24. What are the Visual Meteorological Conditions (VMC) requirements in class D airspace? If you believe you may exceed these requirements on your circuit solo, what actions will you take?
  * *see VFRG > Operations > General information*

25. On the aerodrome you see a white cone with a red band. What does this mean?
  * *see VFRG > General > Aerodrome Markings*

26. You consumed a tiny sip of alcohol last night at 1100pm. Your solo is planned for 0630am the next morning. Is this flight legal?
  * *see Civil Aviation Safety Regulations 1988 - REG 91.570*

27. On the taxiway, you are approaching a series of lines across your path. They are yellow, with a solid line on your side, and a dashed line on the other side. You are unsure if you should cross this line. Describe your next actions.

28. If the pitot tube somehow became blocked during your circuit solo, which instrument(s) would be affected? What symptoms might you see?

29. On an airspeed indicator, what does the bottom end of the *Green* section mean?

30. You need to add fuel before your circuit solo. What type of fuel does your aircraft use? What symptoms might you experience if you used an incorrect fuel?
  * *see the Aircraft Flight Manual GENERAL INFORMATION*

----

##### Administration *(for RA Students)*

* [ ] I have completed membership to the Recreational Aviation Australia and sent details to my instructor
* [ ] I have completed application for a CASA Aviation Reference Number and sent details to my instructor
* [ ] I have completed a Basic Class 2 Medical *(or higher)* and sent details to my instructor
* [ ] I have completed an English Language Proficiency test and sent details to my instructor
* [ ] I have completed the RA-Aus Pre-solo Operational Exam *(5 questions)*
* [ ] I have completed the RA-Aus Pre-solo Air Legislation Exam *(20 questions)*
* [ ] I have completed the RA-Aus Radio Exam *(30 questions)*

##### Administration *(for GA Students)*

* [ ] I have completed application for a CASA Aviation Reference Number and sent details to my instructor
* [ ] I have completed a Basic Class 2 Medical *(or higher)* and sent details to my instructor
* [ ] I have completed an English Language Proficiency test and sent details to my instructor
* [ ] I have completed the CASA Pre-solo Exam
* [ ] I have completed the Aeronautical Radio Operator's Certificate (AROC) Exam

----

##### Written Exam Preparation

* [ ] I've got my own study method and am prepared for the written exams
* [ ] I am using the Aviation Theory Centre (red) BAK and (blue) Radio text books in preparation for the written exams
* [ ] I'd like a reference to additional study material that I can purchase for the written exams
* [ ] I'd like to hire an instructor for an hour or two on ground theory preparation for the written exams
