## Crosswind Circuits

1. What is the name of the circuit leg after downwind?

2. What is the direction of the circuit on RWY 10R during tower hours?

3. What is the name of the circuit leg before crosswind?

4. If wind direction is from the 10 o'clock position while taxiing, what is the correct aileron and elevator input?

5. If wind direction is from the 5 o'clock position while taxiing, what is the correct aileron and elevator input?

6. If wind direction is from the right during the take-off roll, what is the correct aileron input?

7. If wind direction is from the left on final during a crab technique tracking on the runway centreline, which direction will the aircraft be heading?

8. What are the two landing techniques in a crosswind?
