## Circuits

1. What are the names of the each circuit leg?

2. What is the circuit direction at Archerfield during tower hours for
  * RWY 28R?
  * RWY 10L?
  * RWY 04R?

3. What is the circuit direction outside Archerfield tower hours?

4. What are the Archerfield tower hours *(ERSA)*?

5. What is BUMFISH?

6. What is the standard circuit direction?

7. How is a holding point marked on a paved surface?

8. What does a double white + at an aerodrome indicate?

9. If a runway is facing 351°M what is its number designation?

10. What factors affect required take-off distance?
