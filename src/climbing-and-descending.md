## Climbing and Descending

1. Using your Basic Aeronautical Knowledge text book draw the forces acting on an aircraft in the climb and descent?
  * climbing
  * descending

2. How does weight affect our angle of climb?

3. How does weight affect our angle of descent?

4. How do we enter and exit a climb?

5. What are the Vx, Vy and cruise climb expected speeds for your aircraft and the associated attitudes?

6. What are the three types of descents in your aircraft and the power settings & attitudes?

7. What is the elevation of Archerfield airport *(see ERSA)*?

8. You are tracking inbound to Archerfield from Target at 1500ft. What is your radio call?

9. When must you drain the fuel from the aircraft to check for water *(see Civil Aviation Order (CAO) 20.2)*?

10. What is the take off safety speed for your aircraft *(see PoH)*?

11. What is the required minimum amount of oil in your aircraft prior to flight *(see PoH)*?

12. What is a VHF radio and how does it work?

13. What is carburetor heat and should it be used in descents?

14. Looking at the Brisbane/Sunshine Coast Visual Terminal Chart (VTC), where are the eastern and southern training areas?

15. Looking at the Brisbane/Sunshine Coast Visual Terminal Chart (VTC), using the legend, what does C LL 2000 mean and the area frequency to monitor when in the eastern training area?
