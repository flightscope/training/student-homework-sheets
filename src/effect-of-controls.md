## Effect of Controls

* Watch the following videos on the pre-flight inspection of the Eurofox 3K
  * [https://www.youtube.com/watch?v=vVzLe_AKLXM](https://www.youtube.com/watch?v=vVzLe_AKLXM) Eurofox
  * [https://www.youtube.com/watch?v=tMT-bYU-CEM](https://www.youtube.com/watch?v=tMT-bYU-CEM) Rotax 912ULS

1. What is the primary effect of the elevator, aileron and rudder?

2. What are the secondary effects of the elevator, aileron and rudder?

3. When we increase power what happens?

4. When we decrease power what happens?

5. What do the flaps do and what is the maximum flap extension speed?

6. What type of trim do we have and how does it work?

7. Using the Eurofox 3K Flight Manual (PoH)
  * What is the empty weight of your aircraft?
  * What is the maximum take off weight of your aircraft?
  * What is the type of engine in your aircraft?
  * What is the type and capacity of the fuel for your aircraft?

8. Using the Archerfield AIP ERSA
  * What are the runways at Archerfield available for use?
  * What are the names of the taxiways?
  * What is the ground frequency?
