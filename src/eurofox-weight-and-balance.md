## Eurofox Weight and Balance

1. What are the risks associated with loading an aircraft with its C.G. Position beyond its aft limit?

2. What are the risks associated with loading an aircraft with its C.G. Position beyond its forward limit?

3. What are the risks associated with loading an aircraft above its Maximum Take Off Weight (MTOW)?

4. If you are flying and encounter turbulence, is more or less fuel load in your wing tanks preferred for ensuring the structural integrity of the aircraft?

5. Is it legal to fly an aeroplane above MTOW or beyond C.G. Position limits? *(CASR1998 REG 91.095)*

6. Using **your actual weight**, maximum fuel capacity, maximum baggage, determine the maximum passenger weight you can carry in Eurofox 24-4844. An example is given.

##### Example

* Determine figures from the PoH
  * Note: these figures are taken at the time of this writing **(check your figures)**
  * Basic Empty Weight: `288.5kg`
  * MTOW: `560kg`
  * C.G. Position: `244.8mm`
  * Permitted C.G. minimum: `10.2in` = `259.08mm`
  * Permitted C.G. maximum: `14.3in` = `363.22mm`
  * Fuel arm: `9in` = `228.6mm`
  * Crew arm: `15.8in` = `401.32mm`
  * Baggage arm: `15.0in` = `381mm`
  * Total fuel capacity: `85.1L`
  * Total usable fuel capacity: `83L`
  * Maximum baggage allowance: `20kg`
  * Minimum crew weight: `55kg`
* Fuel specific gravity (SG): `0.72L/kg`
  * *See also (ERSA - CON - CONVERSIONS)*
* My actual weight (example): `75kg`

* Weight Calculations
  * Maximum usable fuel weight: `0.72 * 83 = 59.76kg`
  * Weight of aircraft + me + baggage: `288.5 + 75 + 20 = 383.5kg`
  * Weight of aircraft + me + fuel + baggage: `288.5 + 75 + 59.76 + 20 = 443.26kg`
  * Insert passenger up to aircraft MTOW: `560 - 443.26 = 116.74kg`
  * Weight of aircraft + me + baggage + pax: `383.5 + 116.74 = 500.24kg`
  * Weight of me + pax: `75 + 116.74 = 191.74kg`

* Find moments on each arm
  * Multiply each *weight* by its *arm* to obtain its *moment*
  * Aircraft: `288.5kg * 244.8mm = 70624.8kg/mm`
  * Fuel: `59.76kg * 228.6mm = 13661.136kg/mm`
  * Crew: `191.74kg * 401.32mm = 76949.0968kg/mm`
  * Baggage: `20kg * 381mm = 7620kg/mm`

* Find C.G. Position with no remaining usable fuel
  * Note: This weight is called *Zero Fuel Weight (ZFW)*
  * Add total moments
    * aircraft + crew + baggage
    * `70624.8 + 76949.0968 + 7620 = 155193.8968kg/mm`
  * Final C.G. Position is total moment divided by aircraft weight
    * `155193.8968kg/mm ÷ 500.24kg = 310.239mm`

* Find C.G. Position with full usable fuel
  * Note: This weight is called *All Up Weight (AUW)*
  * Add total moments
    * aircraft + fuel + crew + baggage
    * `70624.8 + 13661.136 + 76949.0968 + 7620 = 168855.0328kg/mm`
  * Final C.G. Position is total moment divided by aircraft weight
    * `168855.0328kg/mm ÷ 560kg = 301.527mm`

* Checks
  * Aircraft AUW is at or below MTOW
    * AUW: `560kg`
    * MTOW: `560kg`
  * C.G. Position is within permitted range at ZFW
    * C.G. Position: `310.239mm`
    * C.G. Permitted Range: `259.08mm-363.22mm`
  * C.G. Position is within permitted range at AUW
    * C.G. Position: `301.527mm`
    * C.G. Permitted Range: `259.08mm-363.22mm`
  * Baggage weight is at or below maximum allowable
    * Baggage: `20kg`
    * Maximum allowable: `20kg`
  * Crew weight is above minimum crew weight
    * Crew weight: `191.74kg`
    * Minimum crew: `55kg`
  * **If one or more checks fail, remove fuel, baggage and/or passenger weight and repeat calculation**

7. Using **your actual weight**, 15kg baggage, a 130kg passenger, determine the maximum amount of fuel you can carry in Eurofox 24-8881. An example is given.

##### Example

* Determine figures from the PoH
  * Note: these figures are taken at the time of this writing **(check your figures)**
  * Basic Empty Weight: `303.8kg`
  * MTOW: `560kg`
  * C.G. Position: `303.82mm`
  * Permitted C.G. minimum: `260.0mm`
  * Permitted C.G. maximum: `442.0mm`
  * Fuel arm: `440.0mm`
  * Crew arm: `440.0mm`
  * Baggage arm: `1200.0mm`
  * Total fuel capacity: `86.0L`
  * Total usable fuel capacity: `85L`
  * Maximum baggage allowance: `20kg`
  * Minimum crew weight: `54kg`
* Fuel specific gravity (SG): `0.72L/kg`
  * *See also (ERSA - CON - CONVERSIONS)*
* My actual weight (example): `85kg`

* Determine Crew Weight
  * me + pax
  * `85 + 130 = 210.0kg`

* Determine Zero Fuel Weight
  * aircraft + crew + baggage
  * `303.8 + 210 + 15 = 533.8kg`

* Insert fuel weight up to MTOW and calculate quantity
  * `560 - 533.8 = 26.2kg`
  * 26.2kg of fuel in litres: `26.2 / 0.72 = 36.39L`

* Add fuel to unusable fuel to obtain total fuel:
  * Unusable fuel = `86 - 85 = 1L`
  * Total fuel = `36.39 + 1 = 37.39L`

* Find moments on each arm
  * Multiply each *weight* by its *arm* to obtain its *moment*
  * Aircraft: `303.8kg * 303.82mm = 92300.516kg/mm`
  * Fuel: `26.2kg * 440.0mm = 11528.0kg/mm`
  * Crew: `210kg * 440.0mm = 92400kg/mm`
  * Baggage: `15kg * 1200mm = 18000kg/mm`

* Find C.G. Position with no remaining usable fuel *(ZFW)*
  * Add total moments
    * aircraft + crew + baggage
    * `92300.516 + 92400 + 18000 = 202700.516kg/mm`
  * Final C.G. Position is total moment divided by aircraft weight
    * `202700.516kg/mm ÷ 533.8kg = 379.731mm`

* Find C.G. Position with planned fuel *(AUW)*
  * Add total moments
    * aircraft + fuel + crew + baggage
    * `92300.516 + 11528 + 92400 + 18000 = 214228.516kg/mm`
  * Final C.G. Position is total moment divided by aircraft weight
    * `214228.516kg/mm ÷ 560kg = 382.551mm`

* Checks
  * Aircraft AUW is at or below MTOW
    * AUW: `560kg`
    * MTOW: `560kg`
  * C.G. Position is within permitted range at ZFW
    * C.G. Position: `379.731mm`
    * C.G. Permitted Range: `260.0mm-442.0mm`
  * C.G. Position is within permitted range at AUW
    * C.G. Position: `382.551mm`
    * C.G. Permitted Range: `260.0mm-442.0mm`
  * Baggage weight is at or below maximum allowable
    * Baggage: `15kg`
    * Maximum allowable: `20kg`
  * Crew weight is above minimum crew weight
    * Crew weight: `210kg`
    * Minimum crew: `54kg`
  * **If one or more checks fail, remove fuel, baggage and/or passenger weight and repeat calculation**

8. Using your weight, a 90kg passenger, full fuel and 12kg baggage, perform all weight and balance checks for your planned flight in aircraft Eurofox 24-5350.
