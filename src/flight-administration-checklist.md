## Flight Administration Checklist

Administration of your flight is important to your flight training. This checklist contains flight administration tasks that any student is permitted to perform on their own initiative at any time throughout their training. A new student is unlikely to know how to perform many of these tasks; your instructor will help you learn these tasks throughout your training until you are able to complete them all unassisted.

Importantly, if you are ever in doubt, **ask for help**.

*Initiative is rewarded. Enjoy your training!*

----

* [ ] Determine which aircraft is to be used for your flight, by asking your instructor or Flightscope Aviation staff.
* [ ] Determine the amount of fuel quantity (litres) of your aircraft
  * [ ] Is the fuel quantity sufficient for your flight?
    * [ ] Yes
    * [ ] No → fuel the aircraft
    * [ ] In either case, inform your instructor of fuel quantity and ask for assistance to refuel
* Using the administration book for the selected aircraft
  * [ ] Is the maintenance release (MR) signed out for the day?
    * [ ] Yes
    * [ ] No → tell your instructor
* Enter your flight details into the aircraft administration book
  * [ ] the date of your flight
  * [ ] the initials of your instructor
  * [ ] your name
  * [ ] the VDO out time
  * [ ] the Tach out time
  * [ ] the Fuel Start quantity (litres)
* Enter your flight details into the office running sheet
  * [ ] the date of your flight
  * [ ] your name
  * [ ] your flight route
  * [ ] the Fuel Start quantity (litres)
  * [ ] the VDO out time
  * [ ] your initials
* Has the aircraft had a daily inspection?
  * [ ] Yes → perform a **pre-flight** aircraft inspection
  * [ ] No → perform a **daily** aircraft inspection
* Onboard the aircraft
  * [ ] the flight manual
  * [ ] an aeronautical chart that is up to date
  * [ ] your aviation medical (if you have one)
  * [ ] the PLB is up to date
  * [ ] the aircraft registration is up to date
  * [ ] the maintenance release (MR) is signed by your instructor
  * [ ] I.M.S.A.F.E.
* After your flight
  * [ ] Enter the VDO in time into the aircraft administration book
  * [ ] Enter the VDO total time into the aircraft administration book
  * [ ] Enter the Tach in time into the aircraft administration book
  * [ ] Enter the Tach total time into the aircraft administration book
  * [ ] Enter the Fuel end quantity (litres) into the aircraft administration book
  * [ ] Enter the VDO in time into the office running sheet
  * [ ] Enter the VDO total time into the office running sheet
  * [ ] Enter the payment method into the office running sheet
