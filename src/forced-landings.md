## Forced Landings

1. What are some causes of engine failure?

2. What is the initial immediate action on engine failure?

3. You are gliding at your aircraft's best glide speed on profile for a golf course. You then increase the nose attitude. Will you arrive before, at, or after the golf course?

4. Describe the High Key and Low Key positions in a glide approach.

5. You are on profile for Low Key for a field and turn base, but you notice that you are too low to make your field. What actions might you take?

6. Your engine fails at 95KIAS and you immediately set the glide attitude and trim the aircraft. Will the aircraft remain trimmed as it slows to the best glide speed?

7. You have a field in sight for a glide approach, but unsure if it is suitable. What criteria will you use to determine its suitability?

8. How might you determine the current wind direction to select a suitable field for a forced landing?

9. What happens to rate of descent with an increased head wind?

10. What happens to angle of descent with an increased head wind?

11. What happens to angle of descent with flap extension?

12. What is the correct format for a radio call with an engine failure?

13. What troubleshooting checks will you perform if your engine fails?

14. If your engine fails during take-off at 200ft AGL, what are your suitable forced landing options?

15. What is your personal minimum altitude to turn back if you have an engine failure during take-off?

16. How regularly do you brief yourself (or your instructor) for your personal minimum altitude to turn back if you have an engine failure during take-off?

17. What are the risks associated with turning back to the runway, if you have an engine failure during take-off?

18. What is the best glide speed for your most familiar aircraft?

19. If you have an engine failure with a passenger on board, what will you say to your passenger?

20. You are on a short final for a forced landing. How will you secure the aircraft?
