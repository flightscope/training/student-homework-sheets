## Maintenance Release

###### Reference: [*Civil Aviation Advisory Publication 43-01*](https://www.casa.gov.au/sites/default/files/caap-43-01-maintenance-release.pdf)

1. What is the Class of (all) the aircraft at Flightscope Aviation? *(1.2 Definitions)*

2. What is a **major defect**? *(1.2 Definitions)*

3. After your flight, you discover a major defect. What do you do? *(4.2 Dealing with defects)*

4. You are going flying early in the morning and have performed your daily inspection of the aircraft. What next? *(5.1 Daily Inspection)*

5. You are going flying early in the morning and note that the previous day closed out the maintenance release with 266.7 hours. The first page of the maintenance release requires an oil change at 264.3 hours and you see it has not been signed by a mechanic. What do you do?

6. You have completed flying for the day and put the aircraft away. The aircraft did 2.6 hours today and 3 landings, and the running total time is 398.2 hours with 2381 landings. What will you do with the maintenance release?

7. You are about to go flying, but you left the maintenance release in the office. You know it has been signed out for the day. Will you get it after your flight?

8. In your pre-flight inspection, you see in a note in the maintenance release that the airspeed indicator is unserviceable. What will you do?

9. In your pre-flight inspection, you see in a note in the maintenance release that the engine tachometer is unserviceable. What will you do?

10. You rent an aircraft from your friend and conduct its last flight of the day as pilot in command. Who is responsible for closing the maintenance release for the day? *(5.2 Flight time)*

