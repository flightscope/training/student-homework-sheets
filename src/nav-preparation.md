## Navigation training preparation

*This preparation homework provides some items to purchase and documents to read. It is beneficial to you as a student to do this prior to your first navigation briefing. If you get stuck, no problem — just ask your instructor.*

----

1. Brisbane/Sunshine Coast/Gold Coast Visual Terminal Chart (VTC) *(approx $12)*.

2. Rockhampton/Oakey/Brisbane Visual Terminal Chart (VTC) *(approx $12)*.

3. Sydney/Brisbane Visual Navigation Chart (VNC) *(approx $12)*.

3. Bundaberg/Rockhampton Visual Navigation Chart (VNC) *(approx $12)*.

4. Brisbane World Aeronautical Chart (WAC) *(approx $12)*.

5. Armidale World Aeronautical Chart (WAC) *(approx $12)*.

  *Suggestion: Since WAC does not go out of date, cover your WAC with clear adhesive contact paper and use a permanent marker when writing on it. Use alcohol wipes to erase.*

6. Planning Chart Australia (PCA) *(approx $12)*.

7. E6B Flight Computer *(approx $40)*.

  *Suggestion: Purchase a non-permanent fine-point marker for writing on your E6B.*

8. Navigation Scale Ruler including measurements for WAC, VTC, VNC *(approx $15)*.

9. Navigation protractor *(approx $15)*.

10. A means by which to draw on aeronautical charts:
  * 2B pencil, eraser and sharpener OR
  * Erasable gel pen
    * called **Pilot Frixion** pens
    * recommend black, green, red
    * Available at Officeworks, Oxley

11. A means by which to manage charts, pens, etc in the cockpit:
  * e.g. A5 clipboard or kneeboard e.g. Flyboys Reversible Kneeboard.
  * any larger than A5 is _not recommended_.
  * *it is important that you are able to easily access items and write on them during flight*.

12. A means by which to set an alarm that is impossible to ignore. This will be used for managing Search and Rescue times (SARTIME). For example, a watch, phone, or similar — as long as it will **not be ignored**.

13. An accurate time-keeping piece (e.g. a watch) that clearly displays hours, minutes and seconds.

14. Print several (~5) of the **Back Page** of the Airservices Flight Notification Form *[\<link\>](https://www.airservicesaustralia.com/flight-briefing/flight-notification-form/)*.

15. If you know your navigation route:
  * Print in A5 size ERSA (FAC & RDS) entries for destination aerodromes on your route, including those that are enroute within 10NM.
  * Identify destination aerodromes on VTC, VNC and WAC aeronautical charts as you are able to.
  * Identify Restricted and/or Danger areas on your navigation route and find the details for each in the ERSA (PRD AREAS) *[\<link\>](https://www.airservicesaustralia.com/aip/aip.asp)*.

16. Read and familiarise with the following documents *(available on the CASA website)*:
  * CASR1998 Part 91 Manual of Standards Chapter 19 Fuel Requirements.
  * AC 91-10 Operations in the vicinity of non-controlled aerodromes.
  * CAR1988 regulations 166, 166A, 166B, 166C, 166D, 166E.

17. Familiarise with the following sections of the Visual Flight Rules Guide (VFRG) *[\<link\>](vfrg.casa.gov.au/)*:
  * PRE-FLIGHT PLANNING → Meteorology *(all)*.
  * PRE-FLIGHT PLANNING → Preparation → Take-off and landing requirements.
  * PRE-FLIGHT PLANNING → Preparation → Alternate due to weather.
  * PRE-FLIGHT PLANNING → Preparation → Fuel planning.
  * PRE-FLIGHT PLANNING → Preparation → Daylight and darkness.
  * OPERATIONS → Cruising level requirements *(all)*.
  * OPERATIONS → Non-controlled aerodromes → Circuit procedures.

18. Familiarise with submission of an online Flight Notification Form. You may experiment with filling out this form and then press **Save As**. This will save the form within your personal NAIPS account but will not submit it to airservices. Your instructor will help you with this form before submitting to airservices. This form has a link to documentation on the meaning of each field. To access this form:
  * Login to NAIPS *[\<link\>](https://www.airservicesaustralia.com/naips/)*.
  * On the left menu, select **Flight Notification**.
  * Select **ICAO**.

19. Familiarise with alternate aerodrome and fuel planning requirements.
  * VFRG PRE-FLIGHT PLANNING
  * AIP ENR 11.7

20. Familiarise with the E6B flight computer:
  * on the wind side, there are instructions for calculating heading (HDG) and ground speed (GS), given track (TRK), wind direction/speed and true airspeed (TAS). Practice an example, such as: TRK340, wind 01012KT, TAS 90KT; what is the HDG and GS?
  * read the instruction book that came with the E6B flight computer about the calculator side and practice an example.

----

## Navigation Checklist

Create a checklist of planning items to have been completed **prior** to arriving for your navigation exercise. Here is an example checklist:

##### Any time prior to navigation exercise

1. VTC, VNC, WAC
  * Mark planned route
  * 5NM marks on VTC
  * 10NM marks on VNC and WAC
  * Any other marks you think relevant e.g.
    * radio boundaries on WAC along planned route
    * airspace boundaries on WAC along planned route
  * Anticipate any potential diversions on your navigation route
2. Complete and save (not submit) a Flight Notification Form in NAIPS with your flight plan details *[\<link\>](https://www.airservicesaustralia.com/flight-briefing/flight-notification-form/)*
3. Navigation log with all fields completed
  * PSN
  * LSALT
  * FL or ALT
  * TAS *(N0090 for Eurofox)*
  * TR(M)
  * HDG(M) *(requires obtaining current winds from GPWT)*
  * G/S *(requires obtaining current winds from GPWT)*
  * DIST
  * ETI *(requires obtaining current winds from GPWT)*
  * EET *(requires obtaining current winds from GPWT)*
4. Radio log
  * STN
  * COM
  * Anticipate any potential diversions on your navigation route
5. Fuel log *(enter fuel endurance at departure)*
  * TIME
  * ENDURANCE *(minutes)*
6. Fuel plan (see the **Back Page** of the Airservices Flight Notification Form *[\<link\>](https://www.airservicesaustralia.com/flight-briefing/flight-notification-form/))*
* Top of Descent (TOD) plan
  * Mark planned TOD on relevant aeronautical chart(s)
  * 500ft/min descent rate
7. Print (A5) ERSA entries for your destination aerodromes and any aerodromes within 10NM of your planned route
  * Study the ERSA entry for any non-standard procedures
  * Familiarise with aerodrome frequencies *(note on your radio navigation log)*
8. TODR/LDR
  * Take-off and Landing distances required at MTOW
  * CAO 20.7.4(6.1) *(don't forget!)*
9. Weight and Balance
  * at MTOW
  * at ZFW
10. Last light/first light
  * Calculate any last light and first light requirements on your planned navigation route
  * VFRG PRE-FLIGHT PLANNING
  * AIP GEN 2.7
11. Determine and plan any alternate aerodrome requirements
  * VFRG PRE-FLIGHT PLANNING → Preparation → Alternate due to weather
  * AIP ENR 11.7
12. Survival and comfort
  * Warm clothes (shelter)
  * Fully-charged phone
  * Torch
  * Water
  * Food
  * Be familiar with the onboard ELT

##### On the evening or morning prior to the navigation exercise

1. Obtain a weather briefing
  * TAF at each destination aerodrome *(carry it with you onboard)*
  * METAR at each destination aerodrome
  * GAF
  * GPWT
2. Complete navigation log using the forecast winds
  * HDG
  * G/S
  * ETI
  * EET
3. Complete fuel plan
4. Obtain a full briefing for destination aerodromes and head office, including NOTAM. For example:
  * Is the RWY at your destination aerodrome serviceable?
  * Is the Wind Direction Indicator at your destination aerodrome serviceable?
  * Is there any drone (UAS) activity near your destination?
  * Head Office is `YBBB` or `YMMM` *(VFRG PRE-FLIGHT PLANNING)*
  *[\<link\>](https://www.airservicesaustralia.com/naips/)*
5. Nominate an appropriate SARTIME

##### Immediately before departure

1. Complete and submit Flight Notification Form on NAIPS
2. Write navigation route details on office whiteboard
3. Ask instructor any remaining questions regarding your planning

##### Other Considerations

1. Anticipate that the plan may change during flight. For example, you encounter weather and decide to divert to an unplanned aerodrome.
  * Ensure radio log has correct frequencies
  * Note any special procedures (ERSA) at the diverted aerodrome
2. Ensure you conduct and complete CLEAROF checks during flight
  * Do this on your own initiative when able — don't wait for your instructor to tell you
  * If you are asked your fuel endurance and position at any time, ensure you can quickly and accurately answer
3. Although your planned navigation route may not be through CTA, anticipate this possibility
  * e.g. Amberley CTA becomes active at short notice. If your navigation route is nearby YAMB, have the ATIS easily available *(radio navigation log?)*
  * Amberley ATIS is available on the ground by phone (ERSA)
4. Study the airspace on your planned navigation route in detail
  * Be confident of changing planned routes or nominated altitudes by familiarising with airspace
  * Is Amberley CTA active right now?
5. Plans change. Your aeroplane may be unserviceable after a full-stop landing at a destination aerodrome. Do you have a plan?

##### Most Importantly

If you have **any** problems or concerns with this checklist or planning requirements, **ask your instructor ASAP**. For example, at any time, you can contact your instructor and ask for "2 hours of briefing on navigation" if that is what you believe you need. It is an important personal attribute of a pilot to take the initiative and speak up when they are unsure (c.f. Human Factors). Take the initiative and enjoy your learning!
