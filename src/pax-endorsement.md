## Passenger Endorsement

###### Reading Material

* Civil Aviation Order (CAO) 20.11
* Civil Aviation Order (CAO) 20.16.3
* RA-Aus Operations Manual 2.07
* Part 91 Manual of Standards Chapter 20 SAFETY OF PERSONS AND CARGO ON AIRCRAFT
* Civil Aviation Safety Regulations 1998 (CASR1998) 91.155
* Civil Aviation Safety Regulations 1988 (CASR1998) 91.565
* Civil Aviation Safety Regulations 1988 (CASR1998) 91.780
* Civil Aviation Safety Regulations 1988 (CASR1998) 91.785
* Civil Aviation Safety Regulations 1988 (CASR1998) 91.790
* Civil Aviation Safety Regulations 1998 (CASR1998) REG 61.395
* Civil Aviation Safety Regulations 1998 (CASR1998) REG 141.295
* CASA Advisory Circular 91-19 *(multi-part)* Passenger Safety Information
* [Visual Flight Rules Guide (VFRG)](https://www.casa.gov.au/search-centre/plain-english-guides/visual-flight-rules-guide)

----

**Ensure you have a minimum of 10 hours pilot in command prior to your passenger endorsement flight test**

----

1. Read, review and understand RA-Aus Operations Manual, Section 2.07 *10. PASSENGER CARRYING ENDORSEMENT (PAX)*

2. Read, review and understand Civil Aviation Safety Regulation 91.565.

3. Read, review and understand Part 91 Manual of Standards Chapter 20 SAFETY OF PERSONS AND CARGO ON AIRCRAFT.

4. Read, review and understand Civil Aviation Safety Regulations 1998 (CASR1998) REG 61.395.

5. You have arrived in the apron with your passenger, preparing your aircraft. There are aircraft taxiing just outside of the apron. What dangers will you alert your passenger to?

6. How will you brief your passenger on how to approach and move away from the aircraft on the ground?

7. Prepare a brief for your passenger in your most familiar aircraft on the danger points of an aircraft, including:
  * propeller
  * pitot tube
  * wing struts
  * landing gear

8. How will you brief your passenger on the operation of the door(s) of your most familiar aircraft?
  * what about for an aircraft that has a canopy which can only be unlatched on one side?

9. Prepare a brief for your passenger on the operation of the seat-belt or harness for your most familiar aircraft.
  *  *[CAO 20.16.3 (3)]*
  *  *[CAO 20.16.3 (4)]*

10. Your aircraft is fitted with dual flight controls that cannot be easily removed. Prepare a brief for your passenger as it relates to the flight controls on their side.
  * *[CAO 20.16.3 (11)]*

11. Prepare a brief for your passenger on the operation of their headset.

12. Prepare a brief for your passenger on the placement of baggage and the handling of any loose objects. *[CAO 20.11(14)]*

13. Your passenger has entered the aircraft and fastened their seat belt. What checks will you do?

14. Your aircraft is fitted with a Personal Locator Beacon and a fire extinguisher. How will you brief your passenger?
  * *[CAO 20.11 (14)]*

15. Your aircraft is fitted with recline-adjustable seats. You are preparing to land back at Archerfield with a passenger. What pre-landing checks will you perform?
  * *[CAO 20.16.3 (5)]*

16. You are conducting your private flight with your passenger and you are at 3500ft AGL. Your passenger has never controlled an aeroplane before. You feel comfortable in letting your passenger have a "little go of the controls." Is this legal?
  * *[CASR1998 REG 91.155]*
  * What if you are taxiing on the ground and let your passenger control the aircraft?

17. Your passenger is curious about what you learned during your flight training. So you decide to show them a simulated forced landing. Is this legal?
  * *[CASR1998 REG 249]*
  * *[CASR1998 REG 141.295]*

18. You've invited your friend on a weekend flight and they arrive a bit late. Your passenger tells you it was a big night on the drink, and you quickly determine that they are still intoxicated. What will you do?
  * *[CASR1998 REG 91.790]*

19. Your passenger has brought a 6-pack of alcoholic beer with them.
  * *[CASR1998 REG 91.520]*
  * *[CASR1998 REG 91.785]*
  * *[CASR1998 REG 91.790]*
  * Is your passenger allowed to open a beer and drink it during your flight?
  * Are you allowed to have a little sip?

20. You are at cruise with your passenger in an aircraft that is **not** fitted with a "No Smoking" sign or placard. Your passenger lights up a cigarette. What will you do?
  * *[CASR1998 REG 91.530]*

21. What are your flight currency requirements to carry a passenger?
  * *[CASR1998 REG 61.395 (1)]*

22. You are taking a passenger who has difficulty entering and exiting the aircraft due to a disability. How will you prepare your passenger?
  * *[VFRG]*
  * *[CAO 20.11 (14.1.2)]*

23. Your aircraft is fitted with life-jackets in preparation for a possible ditching. Prepare a briefing for your passenger on the use of flotation devices.
  * *[VFRG Passengers - safety briefings and instructions]*
  * *[CAO 20.11 (14)]*

24. You have conducted a flight with your passenger to Dalby, but along the way, you determine that you have made a planning error in calculating last civil twilight. There are 30 minutes remaining. You choose to conduct a Precautionary Search and Landing into a field and there are no houses nearby. How will you manage this situation?
  * *[Advisory Circular 91-19]*
