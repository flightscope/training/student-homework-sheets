## Precautionary Search & Landing

1. What are some reasons to perform a precautionary search & landing?

2. You have miscalculated, low on fuel and expect the engine to stop at any moment. There is a field below you. Describe your plan.

3. Daylight is fading and you have decided to perform a precautionary search & landing into a field. What is your radio call?

4. What are the criteria to consider for selecting a field for precautionary search & landing?

5. What are some airmanship risks and considerations when performing a precautionary search & landing at 200ft AGL?
