## RPC exam preparation

**1. What are the drug & alcohol regulations?**

  * Read _CASR1998 91.520, 91.525, 91.785, 91.790(1)_
  * Read _CASR1998 99.010 Definitions for "permitted level"_

----

**2. What are the minimum VFR aircraft instrument requirements?**

  * Read _CASR1998 Part 91 Manual of Standards Chapter 26 Equipment_

```haskell
{-
An aeroplane for a VFR flight by day must be fitted with equipment
for measuring and displaying the following flight information:
* indicated airspeed
* pressure altitude
* magnetic heading
* time
* Mach number — but only for an aeroplane with operating
  limitations expressed in terms of Mach number
* turn and slip — but only for an aeroplane conducting an aerial
  work operation
* outside air temperature — but only for an aeroplane conducting
  an aerial work operation from an aerodrome at which ambient air
  temperature is not available from ground-based instruments.
Requirements:
* Pressure Altitude must:
  * have an adjustable datum scale calibrated in millibars or hPa
  * be calibrated in ft, except that, if a flight is conducted in
    a foreign country which measures FLs or altitudes in metres,
    the equipment must be calibrated in metres, or fitted with a
    conversion placard or device.
* Magnetic heading must be:
  * a direct reading magnetic compass; or
  * both:
    * a remote indicating compass; and
    * a standby direct reading magnetic compass.
* Time must:
  * display accurate time in hours, minutes and seconds.
  * Must be:
    * fitted to the aircraft; or
    * worn by, or immediately accessible to, the pilot for the
      duration of the flight.
-}

```
----

**3. What are the Visual Meteorological Conditions minima is different airspace classes and the Low Flying minima?**

  * Read _AIP ENR 1.2-1_
  * Read _CASR1998 Part 91 Manual of Standards Chapter 12 Minimum Height Rules_
  * Read _CASR1998 Part 91 Manual of Standards Chapter 2 Definition of VMC Criteria_
  * Read _VFRG Visual Meteorological Conditions[^1]_

```haskell
{-
* Class C <=10000ft AMSL
  * cloud vertical separation >=1000ft
  * cloud horizontal separation >=1500m
  * lateral visibility >=5000m
  * *see also Special VFR*
* Class D <=10000ft AMSL
  * cloud above vertical separation >=500ft
  * cloud below vertical separation >=1000ft
  * cloud horizontal separation >=600m
  * lateral visibility >=5000m
  * *see also Special VFR*
* Class G <=3000ft AMSL or <=1000ft AGL
  * cloud vertical separation, clear of cloud
  * cloud horizontal separation, clear of cloud
  * lateral visibility >=5000m
* Class G >3000ft AMSL and <=10000ft AMSL
  * cloud vertical separation >=1000ft
  * cloud horizontal separation >=1500m
  * lateral visibility >=5000m
* Special VFR [AIP ENR 1.2 (1.2), VFRG 3.7]
  * by day when VMC do not exist, by pilot request, special VFR
    clearance
  * will not unduly delay an IFR flight
  * can comply with CAR1988(157) Low Flying
  * can remain clear of cloud
  * lateral visibility >=1600m
-}
```

----

**4. What is a prohibited, restricted and danger area?**

  * Find one of a prohibited, restricted and danger (PRD) area on the aeronautical charts
  * Find the reference to the PRD area in the ERSA
  * Read _VFRG Prohibited, restricted and danger areas[^2]_

----

**5. What are the requirements for carrying emergency equipment?**

  * Read _CASR1998 Part 91 Manual of Standards Chapter 26 Equipment_
  * Read _CAO 20.11 on the Federal Register of Legislation[^3]_
  * Read _VFRG Safety equipment[^4]_

```haskell
{-
* Life jackets when over water and at a distance greater than that
  which would allow the aircraft to reach land with the engine
  inoperative [CAO 20.11 (5.1)]
* A life jacket shall be stowed at or immediately adjacent to each
  seat. In addition, sufficient additional life jackets or
  individual flotation devices shall be carried in easily
  accessible positions for use by infants or children for whom a
  life jacket or individual flotation device is not available at
  or adjacent to their seated position. [CAO 20.11 (5.1.3)]
* An aircraft that is flown over water at a distance from land
  greater than a distance equal to 30 minutes at normal cruising
  speed, or 100 miles, whichever is the less, must carry, as part
  of its emergency and lifesaving equipment, sufficient life rafts
  to provide a place in a life raft for each person on board the
  aircraft. [CAO 20.11 (5.2.1)]
* If one life raft is required, at least one approved or approved
  portable ELT transmitter and pyro distress signals must be
  carried [CAO 20.11 (6.1)]
* If more than one life raft is required, then at least two ELT
  transmitters and pyro distress signals must be carried
  [CAO 20.11 (6.1)]
* A single engine aircraft must be fitted with, or carry, at least
  1 approved ELT or 1 approved portable ELT if it is:
  [CAO 20.11 (6.2)]
  * on a flight over water; and
  * not required to carry a life raft under paragraph 5.2.1 or
    5.2.2; and
  * either:
    * not equipped with radio communication equipment; or
    * not capable of continuous air‑ground communication.
* For an emergency locator transmitter, emergency position
  indicating radio beacon or personal locator beacon to be an
  eligible ELT, it must meet the following requirements:
  [CASR1998 252A (4)]
  * it must, if activated, operate simultaneously:
    * in the frequency band 406 MHz-406.1 MHz; and
    * on 121.5 MHz;
  * it must be registered with the Australian Maritime Safety
    Authority;
  * if it is fitted with a lithium-sulphur dioxide battery, the
    battery must be of a type authorised by the FAA in accordance
    with TSO-C142 or TSO-C142a.
* To be an approved ELT, an eligible ELT must meet the following
  requirements: [CASR1998 252A (5)]
  * it must be automatically activated on impact;
  * it must be of one of the following types:
    * a type authorised by the FAA in accordance with TSO-C91a for
      operation on 121.5 MHz TSO-C126 for operation in the
      frequency band 406 MHz-406.1 MHz
    * a type that CASA is satisfied is operationally equivalent to
      a type authorised by the FAA and performs at a level that is
      at least equivalent to the level of performance of a type
      authorised by the FAA
-}
```

----

**6. Eurofox 3K**

  * What is the Vne?
```haskell
-- 124KIAS
```
  * What is the Va?
```haskell
-- 95KIAS
```
  * What are the G limits?
    * flaps up
```haskell
-- +4/-2
```
    * flaps down
```haskell
-- +2/0
```
  * What is the maximum AoB?
```haskell
-- 60°
```
  * What is the maximum engine speed?
```haskell
-- 5800rpm not more than 5 minutes
```
  * What is the maximum continuous engine speed?
```haskell
-- 5500rpm
```
  * What is the total and usable fuel capacity?
    * total
```haskell
-- 24-5350: 85.0L
-- 24-8881: 86.0L
```
    * usable
```haskell
-- 24-5350: 83.0L
-- 24-8881: 85.0L
```
    * unusable
```haskell
-- 24-5350: 2.0L
-- 24-8881: 1.0L
```
  * How many fuel tanks?
```haskell
-- two wing tanks 40L each
-- central tank 5L
```
  * Draw the fuel system
```haskell
-- Fuel drain valve
-- Fuel valves (x3)
-- Fuel filter
-- Engine driven fuel pump
-- Electric fuel pump
-- Two carburetors, one per cylinder pair
```
  * What is the Vs0?
```haskell
-- 36KIAS
```
  * What is the Vs1?
```haskell
-- 43KIAS
```
  * What is the Vfe?
```haskell
-- 81KIAS
```
  * What is the maximum demonstrated crosswind?
```haskell
-- 15kt
```
  * What is the block rate of fuel consumption?
```haskell
-- 20L/hour
```
  * What is the basic empty weight (BEW)?
```haskell
-- 24-5350: 303.5kg
-- 24-8881: 303.8kg
```
  * What is the maximum take-off weight (MTOW)?
```haskell
-- 560kg
```
  * What is the maximum baggage weight?
```haskell
-- 20kg
```
  * Describe the engine type and configuration
```haskell
-- Rotax 912ULS
-- four-stroke
-- naturally-aspirated, horizontally-opposed, 4 cylinder
-- centre camshaft with overhead valves
-- water-cooled cylinder heads, air-cooled cylinders
-- dry-sump oil lubrication
-- dual, electronic and capacitor flywheel magneto
-- Maximum power: 100hp
-- Maximum continuous power: 94hp
```
  * What is the propellor gearbox reduction ratio?
```haskell
-- 2.43:1
```
  * Read _the Eurofox 3K Manual[^5]_

----

**7. Perform a Weight and Balance for today's flight**

* Read _the Eurofox 3K Manual[^5]_

----

**8. What is the fuel reserve required for today's flight?**

* Read _VFRG Fuel Requirements[^6]_
* Read _CASR1998 Part 91 Manual of Standards Chapter 19 Fuel Requirements.[^7]_

----

**9. What are the regulations relating to refuelling aircraft?**

* Read _VFRG Refuelling[^8]_
* Read _CAO 20.9 on the Federal Register of Legislation[^3]_

```haskell
{-
* During fuelling operations, the aircraft and ground fuelling
  equipment shall be so located that no fuel tank filling points
  or vent outlets lie: [CAO 20.9 (4.1.1)]
  * within 5 metres (17 ft) of any sealed building; and
  * within 6 metres (20 ft) of other stationary aircraft
  * within 15 metres (50 ft) of any exposed public area
  * within 15 metres (50 ft) of any unsealed building in the case
    of aircraft with a maximum take-off weight in excess of
    5 700 kg (12 566 lb)
  * within 9 metres (30 ft) of any unsealed building in the case
    of aircraft with a maximum take-off weight not exceeding
    5 700 kg (12 566 lb)
* CAO 20.9, Fuelling of Aircraft, Location of Aircraft
  * a sealed building is one which all the external part within 15
    metres (50 ft) of an aircraft's fuel tank filling points or
    vent outlets or ground  fuelling equipment is of non-flammable
    materials and has no openings or all openings are closed.
    [CAO 20.9 (4.1.1.2)]
  * The operator of an aircraft must ensure that avgas is not
    loaded onto an aircraft while passengers are on board, or
    entering or leaving, the aircraft. [CAO 20.9 (4.2.1)]
  * When an external electrical supply is used, the connections
    between that supply and the aircraft electrical system shall
    be made and securely locked before a fuelling operation.
    [CAO 20.9 (4.3.2)]
-}
```

----

**10. When must a seat belt be worn?**

* Read _CAO 20.16.3 on the Federal Register of Legislation[^3]_

```haskell
{-
one member of flight crew must be wearing seat belt at all times
[CAO 20.16.3 (4.2)]
-}
```

----

**11. What is the meaning of NCD, FEW, SCT, BKN, OVC, NSC?**

* Read _Basic Aeronautical Knowledge[^18]_
* Read _VFRG forecasts and reports[^9]_

```haskell
{-
* OKTA is a measurement of cloud coverage of the sky, measured in
  eighths
* NCD: 0 OKTAS
* FEW: 1-2 OKTAS
* SCT: 3-4 OKTAS
* BKN: 5-7 OKTAS
* OVC: 8 OKTAS
* NSC: Nil Significant Cloud
-}
```

----

**12. What are the conditions on today's METAR and TAF reports?**

* Obtain a briefing from NAIPS[^17]
* Are cloud heights in an aerodrome forecast or TAF3, relative to AMSL or a different reference point?
```haskell
{-
Cloud heights in an aerodrome forecast or TAF3 are given relative
to the aerodrome elevation
-}
```
* What is a six digit figure?
```haskell
{-
A six digit figure denotes the day of month, the hour and the
minute in Zulu
-}
```
* Read _VFRG forecasts and reports[^9]_
* Read _VFRG Time[^10]_
* Read _VFRG Aerodrome forecasts and reports[^11]_
* Read _AIP GEN 3.5_

----

**13. How do you read a Graphical Area Forecast?**

* How often is a GAF issued?
```haskell
{-
a GAF is issued 6-hourly at times {2300Z, 0500Z, 1100Z, 1700Z},
between 30 and 60 minutes before validity period
-}
```
* How many images are available on a GAF? Why?
```haskell
{-
a GAF contains two images; one is the current and the other is
next
-}
```
* Produce a briefing for today's flight using the GAF
* Read _VFRG Graphical area forecasts (GAF)[^12]_

----

**14. How do you read a Grid Point Wind Forecast?**

* What do the rows mean in each grid-point?
```haskell
{-
each grid in a GPWT contains six rows at different flight levels
{A010, A020, A050, A070, A100}
-}
```
* Produce a briefing for today's flight using the GPWT
* Read _VFRG Grid point wind and temperature (GPWT) forecasts[^13]_

----

**15. What is magnetic variation?**

```haskell
{-
Magnetic variation is the difference between the true north
  (also known as geographic north) and magnetic north
  true (or geographic) north is where lines of longitude converge
  magnetic north is where a magnetic compass points
-}
```

* Find magnetic variation on an aeronautical chart (e.g. VTC)
* Is the wind direction in a Terminal Area Forecast (TAF) provided relative to true or magnetic north?
```haskell
-- true north
```
* What is the magnetic variation at Archerfield?
```haskell
-- 11°E (ERSA)
```
* Are runway headings relative to true or magnetic north?
```haskell
-- magnetic north
```
* Read _Basic Aeronautical Knowledge[^18]_

----

**16. What is situational awareness?**

* Read _Basic Aeronautical Knowledge[^18]_

```haskell
{-
Situational awareness is he perception of the elements in the
environment within a volume of time and space, the comprehension
of their meaning and the projection of their status in the near
future.

cite: Naderpour, Mohsen, Jie Lu, and Etienne Kerre. "A conceptual
model for risk-based situation awareness." Foundations of
Intelligent Systems. Springer, Berlin, Heidelberg, 2011. 297-306.
-}
```

----

**17. When is last civil twilight today?**

* Read _VFRG Daylight and darkness[^14]_
* Read _AIP GEN 2.7_
* Use NAIPS[^17] to find last civil twilight for various locations

----

**18. What is the meaning of the International Standard Atmosphere (ISA)?**

* Read _Basic Aeronautical Knowledge[^18]_

```haskell
{-
ISA is a standard against which to compare the actual atmosphere
at any point and time.
* Pressure: 1013.25 hPa
* Temperature: +15°C
* Density: 1.225 kg/m³
-}
```

----

**19. What is the pressure altitude at YBAF with a QNH of: [1020, 1009, 1013, 1014]?**

* Read _Basic Aeronautical Knowledge[^18]_

```haskell
{-
YBAF elevation: 65 feet (ERSA)

QNH: 1020 hPa
PALT: -145ft

QNH: 1009
PALT: 185ft

QNH: 1013
PALT:  65ft

QNH: 1014
PALT: 35ft
-}
```

----

**20. What is a carburettor and what does it do?**

* Read _Basic Aeronautical Knowledge[^18]_

```haskell
{-
* A carburetor is a device that mixes air and fuel for internal
  combustion engines to a desired air and fuel ratio for
  combustion.
* Carburetor heat redirects air flow from an alternate air intake
  vent that passes by a heat exchange (often, the exhaust
  manifold), to assist in melting ice that may have built up in
  the carburetor throat/intake
* Fuel tank vents equalise the outside pressure with the pressure
  inside the fuel tank. This prevents tank rupturing and assists
  in the flow of fuel from the tank to the fuel system for the
  engine.
-}
```

----

**21. What are the privileges and limitations of the RPC?**

* What is the minimum medical standard for RPC?
```haskell
{-
A health standard equivalent to that required for the issue of a
private motor vehicle licence in Australia; unless operating
within class D airspace under an exemption, which case, a
CASA-issued Basic Class 2 or higher medical.
-}
```
* What is the minimum aeronautical experience to obtain RPC?
```haskell
{-
15 hours dual flight
5 hours solo flight
-}
```
* How often must a RPC holder perform a flight review?
```haskell
{-
Every 24 months
-}
```
* What is the MTOW limitation for RAAus aircraft?
```haskell
{-
if the aeroplane is not equipped to land on water - 600 kg; or
if the aeroplane is equipped to land on water - 650 kg;
[CAO 95.55 (1)]
-}
```
* Read _RAAus Operations Manual 2.01, 2.06, 2.07[^15]_

```haskell
{-
A Pilot Certificate authorises the holder to act as pilot in
command of a recreational aeroplane when:
  * the requirements of Section 2.01 of the manual are met in
    respect of:
    * medical fitness
    * valid membership of the RAAus
    * flight review currency
    * pilot recency
    * the aeroplane is of the same Group shown on the Pilot
      Certificate
    * the pilot ensures their competency to operate the aeroplane
      type
[RAAus Operations Manual 2.07]

In order to act as pilot in command of a recreational aeroplane at
a distance greater than 25 nautical miles from the original point
of departure a Pilot Certificate holder must hold a RAAus Cross
Country (X) Endorsement. Note: Consecutive flights of 25 nautical
miles do not comply with this requirement.
[RAAus Operations Manual 2.01(5)]
-}
```

----

**22. What documents are required to be carried in flight?**

* Read _CASR1998 regulation 61.420 on the Federal Register of Legislation[^3]_
* Read _CASR1998 regulation 91.105, 91.110 on the Federal Register of Legislation[^3]_

```haskell
{-
* pilot licence [CASR1998 61.420]
* pilot medical certificate [CASR1998 61.420]
* aircraft certificate of registration [CAR1988 139 (1)]
* aircraft certificate of airworthiness [CAR1988 139 (1)]
* aircraft maintenance release [CAR1988 139 (1)]
* aircraft flight manual [CAR1988 139 (1)]
-}
```

----

**23. What special procedures apply at YBAF?**

* Read _ERSA FAC for Archerfield Airport (YBAF) on the Aeronautical Information Package[^16]_

```haskell
{-
* Class G outside TWR hours, 1700-0700 (UTC+10) CTAF [AC 91-10]
  with left circuit only
* Contra-rotating circuit during TWR hours, 0700-1700 (UTC+10)
* Elevation: 65ft
-}
```

----

**24. What special procedures apply at YKRY?**

* Read _ERSA FAC for Kingaroy Airport (YKRY) on the Aeronautical Information Package[^16]_

```haskell
{-
* Right hand circuit RWY 34
* All circuits left hand RWY 05/23
* No dead side of RWY contra-rotating circuits
* Contra-rotating circuit RWY 16/34
* Gliding operations during daylight
* CTAF 127.45
-}
```

----

**25. What does CTAF mean?**

* Read _AC 91-10 on the CASA website[^7]_

----

**26. What is an example of a radio call at a uncontrolled aerodrome?**

* Read _AC 91-10 on the CASA website[^7]_

----

**27. What is a private operation?**

* Read _CAR1988 2(7)(d) on the Federal Register of Legislation[^3]_

```haskell
{-
A private operation is [CAR1988 2(7)(d)]
  * the personal transportation of the owner of the aircraft
  * aerial spotting where no remunerations received by the pilot
    or the owner of the aircraft or by any person or organisation
    on whose behalf the spotting is conducted
  * agricultural operations on land owned and occupied by the
    owner of the aircraft
  * aerial photography where no remuneration is received by the
    pilot or the owner of the aircraft or by any person or
    organisation on whose behalf the photography is conducted
  * the carriage of persons or the carriage of goods without a
    charge for the carriage being made other than the carriage,
    for the purposes of trade, of goods being the property of the
    pilot, the owner or the hirer of the aircraft
  * the carriage of persons in accordance
-}
```

----

### Also review other homeworks sheets

* *ensure you have covered these homework sheets completely*
* Stalling
* Forced Landings
* Circuits
* Circuit Emergencies
* Area Solo
* Weight and Balance

----

### Administration

* [ ] I have completed ≥15 hours dual flight instruction
* [ ] I have completed ≥5 hours solo flight
* [ ] I have completed a Basic Class 2 Medical *(or higher)* and it is current
* [ ] I have completed the RA-Aus Air Legislation Exam *(50 questions)*
* [ ] I have completed the RA-Aus Basic Aeronautical Knowledge Exam *(60 questions)*
* [ ] I have worked with my instructor on any remaining knowledge deficiencies
* [ ] Please **print and bring it with you** on the day of your flight test:
  1. **RA-Aus RPC Pilot Certificate Issue Form**

    [https://www.raa.asn.au/files/download/?id=2834](https://www.raa.asn.au/files/download/?id=2834)
  2. **Flightscope Aviation RPC Flight Test Criteria Form**

    [https://flightscope.gitlab.io/training/forms/rpc-flight-test-criteria.pdf](https://flightscope.gitlab.io/training/forms/rpc-flight-test-criteria.pdf)

----

### Abbreviations and References

* *CAR1988* Civil Aviation Regulation 1988

* *CASR1998* Civil Aviation Safety Regulation 1998

* *CAO* Civil Aviation Order

* *CAAP* Civil Aviation Advisory Procedure

* *AIP* Aeronautical Information Package

* *ERSA* En Route Supplement of Australia

* *RAAus Ops* Recreational Aviation Australia Operations Manual

[^1]: [https://vfrg.casa.gov.au/operations/general-information/visual-meteorological-conditions/](https://vfrg.casa.gov.au/operations/general-information/visual-meteorological-conditions/)
[^2]: [https://vfrg.casa.gov.au/operations/cruising-level-requirements/prohibited-restricted-and-danger-areas/](https://vfrg.casa.gov.au/operations/cruising-level-requirements/prohibited-restricted-and-danger-areas/)
[^3]: [https://legislation.gov.au/](https://legislation.gov.au/)
[^4]: [https://vfrg.casa.gov.au/pre-flight-planning/flights-over-water/safety-equipment/](https://vfrg.casa.gov.au/pre-flight-planning/flights-over-water/safety-equipment/)
[^5]: [https://flightscopeaviation.com.au/student-resources/aircraft/](https://flightscopeaviation.com.au/student-resources/aircraft/)
[^6]: [https://vfrg.casa.gov.au/pre-flight-planning/preparation/fuel-requirements/](https://vfrg.casa.gov.au/pre-flight-planning/preparation/fuel-requirements/)
[^7]: [https://legislation.gov.au/](https://legislation.gov.au/)
[^8]: [https://vfrg.casa.gov.au/general/pilot-responsibilities/refuelling/](https://vfrg.casa.gov.au/general/pilot-responsibilities/refuelling/)
[^9]: [https://vfrg.casa.gov.au/pre-flight-planning/meteorology/aerodrome-forecasts-and-reports/](https://vfrg.casa.gov.au/pre-flight-planning/meteorology/aerodrome-forecasts-and-reports/)
[^10]: [https://vfrg.casa.gov.au/pre-flight-planning/preparation/time/](https://vfrg.casa.gov.au/pre-flight-planning/preparation/time/)
[^11]: [https://vfrg.casa.gov.au/pre-flight-planning/meteorology/forecasts-and-reports/](https://vfrg.casa.gov.au/pre-flight-planning/meteorology/forecasts-and-reports/)
[^12]: [https://vfrg.casa.gov.au/pre-flight-planning/meteorology/graphical-area-forecasts/](https://vfrg.casa.gov.au/pre-flight-planning/meteorology/graphical-area-forecasts/)
[^13]: [https://vfrg.casa.gov.au/pre-flight-planning/meteorology/grid-point-wind-and-temperature-gpwt-forecasts/](https://vfrg.casa.gov.au/pre-flight-planning/meteorology/grid-point-wind-and-temperature-gpwt-forecasts/)
[^14]: [https://vfrg.casa.gov.au/pre-flight-planning/preparation/daylight-and-darkness/](https://vfrg.casa.gov.au/pre-flight-planning/preparation/daylight-and-darkness/)
[^15]: [https://flightscopeaviation.com.au/student-resources/raaus/](https://flightscopeaviation.com.au/student-resources/raaus/)
[^16]: [https://www.airservicesaustralia.com/aip/aip.asp](https://www.airservicesaustralia.com/aip/aip.asp)
[^17]: [https://www.airservicesaustralia.com/naips/](https://www.airservicesaustralia.com/naips/)
[^18]: Basic Aeronautical Knowledge is a text book, by either Aviation Theory Centre or Bob Tait
