## RPC exam preparation

**1. What are the drug & alcohol regulations?**

  * Read _CASR1998 91.520, 91.525, 91.785, 91.790(1)_
  * Read _CASR1998 99.010 Definitions for "permitted level"_

----

**2. What are the minimum VFR aircraft instrument requirements?**

  * _Read CASR1998 Part 91 Manual of Standards Chapter 26 Equipment_

----

**3. What are the Visual Meteorological Conditions minima is different airspace classes and the Low Flying minima?**

  * Read _AIP ENR 1.2-1_
  * Read _CASR1998 Part 91 Manual of Standards Chapter 2 Definition of VMC Criteria_
  * Read _CASR1998 Part 91 Manual of Standards Chapter 12 Minimum Height Rules_
  * Read _VFRG Visual Meteorological Conditions[^1]_

----

**4. What is a prohibited, restricted and danger area?**

  * Find one of a prohibited, restricted and danger (PRD) area on the aeronautical charts
  * Find the reference to the PRD area in the ERSA
  * Read _VFRG Prohibited, restricted and danger areas[^2]_

----

**5. What are the requirements for carrying emergency equipment?**

  * Read _CASR1998 Part 91 Manual of Standards Chapter 26 Equipment_
  * Read _CAO 20.11 on the Federal Register of Legislation[^3]_
  * Read _VFRG Safety equipment[^4]_

----

**6. Eurofox 3K**

  * What is the Vne?
  * What is the Va?
  * What are the G limits?
    * flaps up
    * flaps down
  * What is the maximum AoB?
  * What is the maximum engine speed?
  * What is the maximum continuous engine speed?
  * What is the total and usable fuel capacity?
    * total
    * usable
    * unusable
  * How many fuel tanks?
  * Draw the fuel system
  * What is the Vs0?
  * What is the Vs1?
  * What is the Vfe?
  * What is the maximum demonstrated crosswind?
  * What is the block rate of fuel consumption?
  * What is the basic empty weight (BEW)?
  * What is the maximum take-off weight (MTOW)?
  * What is the maximum baggage weight?
  * Describe the engine type and configuration
  * What is the propellor gearbox reduction ratio?
  * Read _the Eurofox 3K Manual[^5]_

----

**7. Perform a Weight and Balance for today's flight**

* Read _the Eurofox 3K Manual[^5]_

----

**8. What is the fuel reserve required for today's flight?**

* Read _VFRG Fuel Requirements[^6]_
* Read _CASR1998 Part 91 Manual of Standards Chapter 19 Fuel Requirements.[^7]_

----

**9. What are the regulations relating to refuelling aircraft?**

* Read _VFRG Refuelling[^8]_
* Read _CAO 20.9 on the Federal Register of Legislation[^3]_

----

**10. When must a seat belt be worn?**

* Read _CAO 20.16.3 on the Federal Register of Legislation[^3]_

----

**11. What is the meaning of NCD, FEW, SCT, BKN, OVC, NSC?**

* Read _Basic Aeronautical Knowledge[^18]_
* Read _VFRG forecasts and reports[^9]_

----

**12. What are the conditions on today's METAR and TAF reports?**

* Obtain a briefing from NAIPS[^17]
* Are cloud heights in an aerodrome forecast or TAF3, relative to AMSL or a different reference point?
* What is a six digit figure?
* Read _VFRG forecasts and reports[^9]_
* Read _VFRG Time[^10]_
* Read _VFRG Aerodrome forecasts and reports[^11]_
* Read _AIP GEN 3.5_

----

**13. How do you read a Graphical Area Forecast?**

* How often is a GAF issued?
* How many images are available on a GAF? Why?
* Produce a briefing for today's flight using the GAF
* Read _VFRG Graphical area forecasts (GAF)[^12]_

----

**14. How do you read a Grid Point Wind Forecast?**

* What do the rows mean in each grid-point?
* Produce a briefing for today's flight using the GPWT
* Read _VFRG Grid point wind and temperature (GPWT) forecasts[^13]_

----

**15. What is magnetic variation?**

* Find magnetic variation on an aeronautical chart (e.g. VTC)
* Is the wind direction in a Terminal Area Forecast (TAF) provided relative to true or magnetic north?
* What is the magnetic variation at Archerfield?
* Are runway headings relative to true or magnetic north?
* Read _Basic Aeronautical Knowledge[^18]_

----

**16. What is situational awareness?**

* Read _Basic Aeronautical Knowledge[^18]_

----

**17. When is last civil twilight today?**

* Read _VFRG Daylight and darkness[^14]_
* Read _AIP GEN 2.7_
* Use NAIPS[^17] to find last civil twilight for various locations

----

**18. What is the meaning of the International Standard Atmosphere (ISA)?**

* Read _Basic Aeronautical Knowledge[^18]_

----

**19. What is the pressure altitude at YBAF with a QNH of: [1020, 1009, 1013, 1014]?**

* Read _Basic Aeronautical Knowledge[^18]_

----

**20. What is a carburettor and what does it do?**

* Read _Basic Aeronautical Knowledge[^18]_

----

**21. What are the privileges and limitations of the RPC?**

* What is the minimum medical standard for RPC?
* What is the minimum aeronautical experience to obtain RPC?
* How often must a RPC holder perform a flight review?
* What is the MTOW limitation for RAAus aircraft?
* Read _RAAus Operations Manual 2.01, 2.06, 2.07[^15]_

----

**22. What documents are required to be carried in flight?**

* Read _CASR1998 regulation 61.420 on the Federal Register of Legislation[^3]_
* Read _CASR1998 regulation 91.105, 91.110 on the Federal Register of Legislation[^3]_

----

**23. What special procedures apply at YBAF?**

* Read _ERSA FAC for Archerfield Airport (YBAF) on the Aeronautical Information Package[^16]_

----

**24. What special procedures apply at YKRY?**

* Read _ERSA FAC for Kingaroy Airport (YKRY) on the Aeronautical Information Package[^16]_

----

**25. What does CTAF mean?**

* Read _AC 91-10 on the CASA website[^7]_

----

**26. What is an example of a radio call at a uncontrolled aerodrome?**

* Read _AC 91-10 on the CASA website[^7]_

----

**27. What is a private operation?**

* Read _CAR1988 2(7)(d) on the Federal Register of Legislation[^3]_

----

### Also review other homeworks sheets

* *ensure you have covered these homework sheets completely*
* Stalling
* Forced Landings
* Circuits
* Circuit Emergencies
* Area Solo
* Weight and Balance

----

### Administration

* [ ] I have completed ≥15 hours dual flight instruction
* [ ] I have completed ≥5 hours solo flight
* [ ] I have completed a Basic Class 2 Medical *(or higher)* and it is current
* [ ] I have completed the RA-Aus Air Legislation Exam *(50 questions)*
* [ ] I have completed the RA-Aus Basic Aeronautical Knowledge Exam *(60 questions)*
* [ ] I have worked with my instructor on any remaining knowledge deficiencies
* [ ] Please **print and bring it with you** on the day of your flight test:
  1. **RA-Aus RPC Pilot Certificate Issue Form**

    [https://www.raa.asn.au/files/download/?id=2834](https://www.raa.asn.au/files/download/?id=2834)
  2. **Flightscope Aviation RPC Flight Test Criteria Form**

    [https://flightscope.gitlab.io/training/forms/rpc-flight-test-criteria.pdf](https://flightscope.gitlab.io/training/forms/rpc-flight-test-criteria.pdf)

----

### Abbreviations and References

* *CAR1988* Civil Aviation Regulation 1988

* *CASR1998* Civil Aviation Safety Regulation 1998

* *CAO* Civil Aviation Order

* *CAAP* Civil Aviation Advisory Procedure

* *AIP* Aeronautical Information Package

* *ERSA* En Route Supplement of Australia

* *RAAus Ops* Recreational Aviation Australia Operations Manual

[^1]: [https://vfrg.casa.gov.au/operations/general-information/visual-meteorological-conditions/](https://vfrg.casa.gov.au/operations/general-information/visual-meteorological-conditions/)
[^2]: [https://vfrg.casa.gov.au/operations/cruising-level-requirements/prohibited-restricted-and-danger-areas/](https://vfrg.casa.gov.au/operations/cruising-level-requirements/prohibited-restricted-and-danger-areas/)
[^3]: [https://legislation.gov.au/](https://legislation.gov.au/)
[^4]: [https://vfrg.casa.gov.au/pre-flight-planning/flights-over-water/safety-equipment/](https://vfrg.casa.gov.au/pre-flight-planning/flights-over-water/safety-equipment/)
[^5]: [https://flightscopeaviation.com.au/student-resources/aircraft/](https://flightscopeaviation.com.au/student-resources/aircraft/)
[^6]: [https://vfrg.casa.gov.au/pre-flight-planning/preparation/fuel-requirements/](https://vfrg.casa.gov.au/pre-flight-planning/preparation/fuel-requirements/)
[^7]: [https://legislation.gov.au/](https://legislation.gov.au/)
[^8]: [https://vfrg.casa.gov.au/general/pilot-responsibilities/refuelling/](https://vfrg.casa.gov.au/general/pilot-responsibilities/refuelling/)
[^9]: [https://vfrg.casa.gov.au/pre-flight-planning/meteorology/aerodrome-forecasts-and-reports/](https://vfrg.casa.gov.au/pre-flight-planning/meteorology/aerodrome-forecasts-and-reports/)
[^10]: [https://vfrg.casa.gov.au/pre-flight-planning/preparation/time/](https://vfrg.casa.gov.au/pre-flight-planning/preparation/time/)
[^11]: [https://vfrg.casa.gov.au/pre-flight-planning/meteorology/forecasts-and-reports/](https://vfrg.casa.gov.au/pre-flight-planning/meteorology/forecasts-and-reports/)
[^12]: [https://vfrg.casa.gov.au/pre-flight-planning/meteorology/graphical-area-forecasts/](https://vfrg.casa.gov.au/pre-flight-planning/meteorology/graphical-area-forecasts/)
[^13]: [https://vfrg.casa.gov.au/pre-flight-planning/meteorology/grid-point-wind-and-temperature-gpwt-forecasts/](https://vfrg.casa.gov.au/pre-flight-planning/meteorology/grid-point-wind-and-temperature-gpwt-forecasts/)
[^14]: [https://vfrg.casa.gov.au/pre-flight-planning/preparation/daylight-and-darkness/](https://vfrg.casa.gov.au/pre-flight-planning/preparation/daylight-and-darkness/)
[^15]: [https://flightscopeaviation.com.au/student-resources/raaus/](https://flightscopeaviation.com.au/student-resources/raaus/)
[^16]: [https://www.airservicesaustralia.com/aip/aip.asp](https://www.airservicesaustralia.com/aip/aip.asp)
[^17]: [https://www.airservicesaustralia.com/naips/](https://www.airservicesaustralia.com/naips/)
[^18]: Basic Aeronautical Knowledge is a text book, by either Aviation Theory Centre or Bob Tait
