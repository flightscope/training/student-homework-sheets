## RPC Meteorology 

*Using the AIP Gen3.5 answer the following meteorology questions*
*Note: Students should be familiar with obtaining a weather brief via NAIPS*

1. What does a "TEMPO" signify?

2. What does an "INTER" signify?

3. What does "CAVOK" signify?

4. What is the difference between a TAF and a METAR?

5. What does "PROB30" before a weather event signify?

6. What is a "SPECI"?

7. In reports, clouds are indicated by FEW,SCT,BKN,OVC. What is the cloud coverage for each of  the abbreviations?

8. Referencing the AIP ENR 1.1- 2.3, what are the Visual Meteorological Conditions required to maintain VFR flight within Class D airspace?

9. Given the following report 
  * "SPECI YBAF 220445Z 23014G29KT 4000  TSRA FEW010CB BKN020 26/22 Q1003 RMK RF04.0/004.0". 
  * If you had a flight lesson planned at 3pm local, determine if your lesson would or would not go ahead, and why?

10. Obtain the weather briefing on the day of your next flight and decipher/discuss with your instructor