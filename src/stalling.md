## Stalling

1. Where is the chord line of an aerofoil? Draw a diagram of the chord line.

2. What is the Relative Air Flow (RAF) and what is its relationship to Lift?

3. What does Angle of Attack (AoA) mean? Draw a diagram depicting the Angle of Attack.

4. What is the fundamental definition of a stall?

5. What does stall speed mean?

6. What is the stall speed for your most familiar aircraft?

7. Where is the stall speed indicated on your airspeed indicator?

8. In level flight, what happens to the stall speed as we increase Angle of Bank (AoB)?

9. What does Load Factor mean?

10. What is the Load Factor in a 45° AoB level turn? Also 50° and 60°?

11. What are the symptoms of an impending stall?

12. What in-flight checks do we perform before practicing stall recovery training?

13. Can an aeroplane stall in a nose down attitude?

14. What critical steps do we take to recover from stalled flight?

15. If we do not correctly recover a stall, what will happen to the aircraft?

16. An aeroplane with a published stall speed of 47KIAS enters a level 45° AoB turn. What is the new stall speed in the turn?

17. What does adding power do to our stall speed?

18. What does adding weight do to our stall speed?
