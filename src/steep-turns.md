## Steep Turns

1. Describe the workflow to enter a steep turn.

2. What is the Load Factor in a 30° AoB level turn?

3. What is the workflow to maintain during a steep turn?

4. What are some situations where we may need to enter a steep turn?

5. What is the definition of a steep turn?

6. What is the Load Factor in a 45° AoB level turn?

7. You enter a 45° AoB level turn at 100KIAS. You then increase AoB to 60° while still maintaining 100KIAS.
  * What happens to your turn radius?
  * What happens to your rate of turn?

8. You enter a 45° AoB level turn at 90KIAS. You maintain AoB of 45° but increase airspeed to 100KIAS.
  * What happens to your turn radius?
  * What happens to your rate of turn?

10. You are piloting an aircraft with a published stall speed of 40KIAS. You enter a 60° level turn. What is your new stall speed?
