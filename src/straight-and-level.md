## Straight and Level

1. What are the 4 forces acting on an aircraft in flight?

2. In straight and level flight, what are the forces said to be?

3. What is the stability in the 3 different axes of the aircraft you are flying?
  * Lateral axis
  * Longitudinal axis
  * Normal Axis

4. What are the two types of drag affecting the aircraft?

5. Draw the total drag curve.

6. What is the mnemonic we use to set straight and level?

7. What are the power settings (attitude and engine speed) for the three types of cruises in your aircraft?
  * slow
  * normal
  * fast

8. Using the Enroute Supplement of Australia (ERSA) for Archerfield, how do we do an eastern departure from Runway 10L and Runway 28R?

9. What is the northern tower frequency and ATIS frequency for Archerfield?

10. What altitude do we depart Archerfield at and what altitude do we return to Archerfield?

11. How do we recover the aircraft to straight and level flight if it has been disturbed?

12. What is the maximum take off weight (MTOW) of your aircraft?

13. What is the Vx *(best angle of climb)* and Vy *(best rate of climb)* speeds for your aircraft *(see PoH)*?

14. What is the maximum fuel your aircraft can carry and what is the burn rate per hour *(see POH)*?
