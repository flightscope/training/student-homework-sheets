## Turning

1. Draw the forces acting on the aircraft in a turn?

2. What are the three types of turns we have and the corresponding Angle of Bank (AoB)?

3. What does load factor mean?

4. What is a skidding turn?

5. What is a slipping turn?

6. What workflow(s) do we use to enter, maintain and exit a turn?

7. Why does a Rotax engine need coolant and do we check it before flight?

8. You notice a crack in the propeller during your pre-flight inspection. What would you do?

9. How would you conduct a southern departure if the tower gave you runway 28R with a left turn?

10. What is the maximum RPM for the engine in your training aircraft?

11. What is the work cycle for straight and level flight?

12. You are taxiing down taxiway Bravo and there is another aircraft approaching head on. What should each aircraft do?

13. We overfly Archerfield and notice a white dumbbell near the main windsock. What does this mean?

14. What is considered a threat in aviation?

15. If you are flying straight and level and you feel you are holding a lot of forward pressure which way would you move the trim wheel?

16. Describe the symptoms of a spiral descent

17. Describe the actions to recover a spiral descent
