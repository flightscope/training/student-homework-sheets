## Whiz Wheel Exercises

*Use your Whizz Wheel to solve these questions*
*Note: All of these questions can be solved with the S/D/T formula*

1. Your next way-point is 70nm from your current position, how long will your leg be if your ground speed is 95KT?

2. Your ground speed is 60KT, how long will it take you to fly 80nm?

3. Your aeroplane burns 20L/H, how much fuel do you need for a 27 minute leg?

4. You have flown for 100 minutes with a ground speed of 90KT, how far have you flown?

5. It has taken you 20 minutes to fly 50NM, what is your ground speed?

6. It has taken you 12 minutes to fly 15NM, what is your ground speed?

7. Find out your heading (M) and ground speed from the following:
  * Track: 205° (T)
  * TAS: 90KT
  * Wind: 180/15 (T)
  * Var: 10°E

8. Find out your heading (M) and ground speed from the following:
  * Track: 240° (T)
  * TAS: 90KT
  * Wind: 050/10 (T)
  * Var: 10°E

9. Find out your heading (M) and ground speed from the following:
  * Track: 090° (T)
  * TAS: 90KT
  * Wind: 160/15 (T)
  * Var: 10°W

10. Find out your heading (M) and ground speed for the following:
  * Track: 190° (T)
  * TAS: 90KT 
  * Wind: 190/20 (T)
  * Var: 10°E

11. It has taken you 45 minutes to fly 80NM, what is your ground speed?

12. You are about to private hire a Eurofox with a planned fuel burn rate of 20L/H. Your planning requires you to have enough fuel for 130 minutes plus the required FlightScope 45 minutes reserve. How much fuel do you require to carry?

13. You have flown 100nm in 30 minutes, what is your ground speed?

14. If your ground speed is 120KT, how long will it take to cover 40nm?

15. You have flown for 20 minutes with a known ground speed of 105KT. How far have you flown?
